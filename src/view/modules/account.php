<?php
if (isset($_GET["status"])) {
    if (strcmp($_GET["status"], "login_fail") == 0) {
        ?>
        <div class="error box error" style="margin-left: 32px; margin-top: 32px">
            La connexion a échoué. Vérifiez vos identifiants et réessayez.
        </div>
    <?php } elseif (strcmp($_GET["status"], "signin_success") == 0) {
        ?>
        <div class="box info" style="margin-left: 32px; margin-top: 32px">
            Inscription réussie ! Vous pouvez dès à présent vous connecter.
        </div>

    <?php } elseif (strcmp($_GET["status"], "logout") == 0) {
        ?>
        <div class="box info" style="margin-left: 32px; margin-top: 32px">
            Vous êtes déconnecté. A bientôt!
        </div>
    <?php }
} ?>
<div id="account">
    <form class="account-login" method="post" action="/account/login">

        <h2>Connexion</h2>
        <h3>Tu as déjà un compte ?</h3>

        <p>Adresse mail</p>
        <input type="text" name="usermail" placeholder="Adresse mail"/>

        <p>Mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe"/>

        <input type="submit" value="Connexion"/>

    </form>

    <form class="account-signin" method="post" action="/account/signin">

        <h2>Inscription</h2>
        <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

        <p>Nom</p>
        <input type="text" name="userlastname" placeholder="Nom"/>

        <p>Prénom</p>
        <input type="text" name="userfirstname" placeholder="Prénom"/>

        <p>Adresse mail</p>
        <input type="text" name="usermail" placeholder="Adresse mail"/>

        <p>Mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe"/>

        <p>Répéter le mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe"/>

        <input type="submit" value="Inscription"/>

    </form>

</div>

<script src="/public/scripts/signin.js"></script>
