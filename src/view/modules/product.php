<div id="product">
    <div>
        <div class="product-images">
            <img id="imageVue" src="/public/images/<?php echo $params["informations"]["image"] ?>">
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?php echo $params["informations"]["image"] ?>">
                </div>
                <div>
                    <img src="/public/images/<?php echo $params["informations"]["image_alt1"] ?>">
                </div>
                <div>
                    <img src="/public/images/<?php echo $params["informations"]["image_alt2"] ?>">
                </div>
                <div>
                    <img src="/public/images/<?php echo $params["informations"]["image_alt3"] ?>">
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category">
                <?php echo $params["informations"]["Category"] ?>
            </p>
            <h1><?php echo $params["informations"]["name"] ?></h1>
            <p class="product-price">
                <?php echo $params["informations"]["price"] ?>
            </p>
            <form>
                <button type="button" name="-">-</button>
                <button type="button" name="Quantité">1</button>
                <button type="button" name="+">+</button>
                <input type="submit" value="Ajouter au panier">
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?php echo $params["informations"]["spec"] ?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php
            if (count($params["comments"])) {
                foreach ($params["comments"] as $c) {
                    //var_dump($params["comments"]);
                    ?>
                    <div class="product-comment">
                        <p class="product-comment-author"><?php echo $c["firstname"] . " " . $c["lastname"] ?> </p>
                        <p><?php echo $c["content"]; ?></p>
                    </div>
                <?php }
            } ?>
            <?php if (isset($_SESSION["firstname"])){?>
            <form method="post" action="/postComment">
                <input name="comment" type="text" placeholder="Rediger un commentaire">
            </form>
            <?php }?>
        </div>
    </div>
</div>

<script src="/public/scripts/product.js"></script>
