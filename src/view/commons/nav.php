<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php if (isset($_SESSION["firstname"])) { ?>
        <a class="account" href="/account">
            <img src="/public/images/avatar.png">
            <?php //var_dump($_SESSION); echo $_SESSION['firstname'] .' '.$_SESSION['lastname']; ?>
        </a>
        <a href="/store">Panier</a>
        <a href="/account/logout">Déconnexion</a>
    <?php } else { ?>
        <a class="account" href="/account">
            <img src="/public/images/avatar.png">
            Compte
        </a>
    <?php } ?>
</nav>
