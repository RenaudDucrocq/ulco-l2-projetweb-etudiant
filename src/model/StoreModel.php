<?php

namespace model;

class StoreModel
{

    static function listCategories(): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT id, name FROM category";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function listProducts($category, $price)
    {
        $db = \model\Model::connect();
        //Requete SQL
        if ($category == null && $price == null) {
            $sql = "SELECT product.id, product.name, price, image, category.name AS Category
    FROM product 
    INNER JOIN category
    ON product.category = category.id;";
        }else{
            $where = "category.name= ".$category[0];
            if ($where!=null){
                foreach ($category as $c){
                    $where += " OR category.name = ".$c;
                }
            }
            else $where = 1;
            if ($price == "croissant")$order = "price ASC";
            else if ($price == "decroissant")$order = "price DESC";
            else if ($price == null)$order = 1;

            $sql = "SELECT product.id, product.name, price, image, category.name AS Category
    FROM product 
    INNER JOIN category
    ON product.category = category.id
    WHERE '$where'
    ORDER BY '$order';";
        }
        //Execution de la requete
        $req = $db->prepare($sql);
        $req->execute();

        //Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function infoProduct(int $id)
    {
        $db = \model\Model::connect();
        //Requete SQL
        $sql = "SELECT product.name, product.price, product.image,product.image_alt1,product.image_alt2,product.image_alt3, product.spec, category.name AS Category
FROM product
INNER JOIN category
ON product.category = category.id
WHERE product.id ='$id'";

        //Execution de la requete
        $req = $db->prepare($sql);
        $req->execute();

        //Retourner les résultats (type array)
        return $req->fetch();
    }

}