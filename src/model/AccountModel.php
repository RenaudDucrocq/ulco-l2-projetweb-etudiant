<?php


namespace model;


class AccountModel
{

    static function check(String $firstname,String $lastname,String $mail,String $password):bool{
        // Connexion à la base de données
        $db = \model\Model::connect();

        $sql = "SELECT * FROM account WHERE account.mail = '$mail';";
        $req = $db->prepare($sql);
        $req->execute();
        $variableUtilisateur = $req->fetchAll();
        //cette variable permet de recuperer l'utilisateur dont le mail correspond si il existe deja
        if (strlen($firstname)<2 || strlen($lastname)<2) return false;
        else if (!filter_var($mail,FILTER_VALIDATE_EMAIL)) return false;
        else if ($variableUtilisateur!=null) return false;
        else if (strlen($password)<6) return false;
        else return true;
    }

    static function signin($firstname,$lastname,$mail,$password):bool{
        $firstname = htmlspecialchars($firstname);
        $lastname = htmlspecialchars($lastname);
        $mail = htmlspecialchars($mail);
        $password = htmlspecialchars($password);
        if (self::check($firstname,$lastname,$mail,$password)){
            // Connexion à la base de données
            $db = \model\Model::connect();

            //$password=password_hash($password,PASSWORD_BCRYPT);
            // Requête SQL
            $sql = "INSERT INTO account (firstname,lastname,mail,password)
            VALUES ('$firstname','$lastname','$mail','$password');";

            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute();
            return true;
        }
        else return false;
    }

    static function login($mail,$password){
        // Connexion à la base de données
        $db = \model\Model::connect();
        $mail = htmlspecialchars($mail);
        $password = htmlspecialchars($password);
        // Requête SQL
        $sql = "SELECT * FROM account WHERE account.mail = '$mail' AND account.password = '$password';";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        return $req->fetch();
    }
}