<?php

namespace controller;

class StoreController
{

    public function store(): void
    {
        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();

        $products=$this->search();


        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "products" => $products
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function product(int $id): void
    {
        // Communications avec la base de données
        $informations = \model\StoreModel::infoProduct($id);
        $comments = \model\CommentModel::listComment($id);

        if ($informations == null) {
            header("Location: /store");
            exit();
        }
        if ($comments == null){
            $comments = "";
        }
        $_SESSION["product_id"] = $id;

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "product.php",
            "informations" => $informations,
            "comments" => $comments
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
        
    }

    public function search(){
        $i = 0;
        if (empty($categoryOn))$categoryOn[0]=null;
        else{
            foreach ($_POST["category"] as $c){
                if (isset($c))$categoryOn[$i] = $c;
                $i++;
            }
        }

        if (isset($_POST["order"])){
            $order = $_POST["order"];
        }else $order = null;

        $products = \model\StoreModel::listProducts($categoryOn,$order);
        return $products;
    }



}