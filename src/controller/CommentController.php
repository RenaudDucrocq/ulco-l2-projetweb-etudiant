<?php


namespace controller;


class CommentController
{

    public function postComment():void{
        if (!isset($_SESSION)){
            header("Location: /home");
            exit();
        }

        $commentToPost = $_POST['comment'];
        $id = $_SESSION["product_id"];

        \model\CommentModel::insertComment(htmlspecialchars($commentToPost),$id,$_SESSION["id"]);
        header("Location: /store/".$id);
        exit();
    }

}