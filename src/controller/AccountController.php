<?php


namespace controller;


class AccountController
{
    public function account(): void
    {
        // Variables à transmettre à la vue
        $params = [
            "title"  => "Home",
            "module" => "account.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function signin():void{
        $firstname = $_POST['userfirstname'];
        $lastname = $_POST['userlastname'];
        $mail = $_POST['usermail'];
        $password = $_POST['userpass'];

        $utilisateur = \model\AccountModel::signin($firstname,$lastname,$mail,$password);
        header("Location: /account?status=signin_success");
        exit();
    }
    public function login():void{
        $mail = $_POST['usermail'];
        $password = $_POST['userpass'];
        $utilisateur = \model\AccountModel::login($mail,$password);
        if ($utilisateur!=null) {
            $_SESSION['id'] = $utilisateur['id'];
            $_SESSION['firstname'] = $utilisateur['firstname'];
            $_SESSION['lastname'] = $utilisateur['lastname'];
            $_SESSION['mail'] = $utilisateur['mail'];
            header("Location: /store");
        }else header("Location: /account?status=login_fail");
    }
    public function logout():void{
        session_destroy();
        header("Location: /account?status=logout");
        exit();
    }
}