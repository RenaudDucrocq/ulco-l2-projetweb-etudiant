document.addEventListener('DOMContentLoaded',function (){
    let miniatures = document.getElementsByClassName("product-images");
    let collImages = miniatures[0].getElementsByTagName("img");
    let moins = document.getElementsByName("-");
    let plus = document.getElementsByName("+");
    let Quantite = document.getElementsByName("Quantité")
    let form = document.getElementsByTagName("form")

    //Modification des images et miniatures
    for (let elem of collImages){
        elem.addEventListener('click',function (){
            collImages[0].src=elem.src;
        })
    }

    //Modification de la quantitée
    //la diminution
    moins[0].addEventListener('click',function (){
        if (parseInt(Quantite[0].innerText)>1){
            let quantite = parseInt(Quantite[0].innerText)-1
            Quantite[0].innerText=quantite;
        }
        if(parseInt(Quantite[0].innerText)==4){
            form[0].lastChild.remove()
        }
    })
    //l'addition
    plus[0].addEventListener('click',function (){
        if(parseInt(Quantite[0].innerText)<5){
            let quantite = parseInt(Quantite[0].innerText)+1
            Quantite[0].innerText=quantite;
        }
        if(parseInt(Quantite[0].innerText)==5){
            let erreur = document.createElement('div');
            erreur.classList.add("box","error");
            erreur.textContent = "Quantité maximale autorisée !";
            if (!form[0].lastChild.isEqualNode(erreur)) {
                form[0].appendChild(erreur);
            }
        }
    })
})