document.addEventListener('DOMContentLoaded',function (){
    let form = document.getElementsByClassName("account-signin")[0];
    let firstname = form.elements["userfirstname"];
    let lastname = form.elements["userlastname"];
    let mail = form.elements["usermail"];
    let password = form.elements["userpass"];
    let childrenForm = form.childNodes;
    console.log(form);
    console.log(childrenForm);

    form.addEventListener('submit',function (event){
        if (!valider()){
            event.preventDefault();//Annule le comportement par défaut de l'élément
        }else{

        }

    })

    firstname.oninput = function (){
        if (firstname.value.length < 3){
            firstname.classList.add("invalid");
            childrenForm[9].classList.add('invalid');
        } else {
            firstname.classList.remove("invalid");
            firstname.classList.add("valid")
            childrenForm[9].classList.add("valid");
            childrenForm[9].classList.remove("invalid");
        }
    }

    lastname.oninput = function (){
        if (lastname.value.length < 3){
            lastname.classList.add("invalid");
            childrenForm[5].classList.add('invalid');
        } else {
            lastname.classList.remove("invalid");
            lastname.classList.add("valid")
            childrenForm[5].classList.add("valid");
            childrenForm[5].classList.remove("invalid");
        }
    }

    mail.oninput = function (){
        if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail.value)){
            mail.classList.add("invalid");
            childrenForm[13].classList.add('invalid');
        } else {
            mail.classList.remove("invalid");
            mail.classList.add("valid")
            childrenForm[13].classList.add("valid");
            childrenForm[13].classList.remove("invalid");
        }
    }

    password[0].oninput = function (){
        password[1].classList.add("invalid");
        childrenForm[21].classList.add("invalid");
        if (!/\d+/.test(password[0].value) || password[0].value.length<6 || !/[a-zA-Z]+/.test(password[0].value)){
            password[0].classList.add("invalid");
            childrenForm[17].classList.add("invalid");
        } else {
            password[0].classList.remove("invalid");
            password[0].classList.add("valid")
            childrenForm[17].classList.add("valid");
            childrenForm[17].classList.remove("invalid");
        }
    }


    password[1].oninput = function (){
        if (password[1].value.localeCompare(password[0].value)!=0){
            password[1].classList.add("invalid");
            childrenForm[21].classList.add("invalid");
        }
        else {
            password[1].classList.remove("invalid");
            password[1].classList.add("valid")
            childrenForm[21].classList.add("valid");
            childrenForm[21].classList.remove("invalid");
        }
    }
    function valider() {
        if (firstname.value.length < 3 || lastname.value.length < 3) {
            return false;
        } else if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail.value)) {
            return false;
        } else if (!/\d+/.test(password[0].value) || password[0].value.length < 6 || !/[a-zA-Z]+/.test(password[0].value)) {
            return false;
        } else if (password[1].value.localeCompare(password[0].value) != 0) {
            return false;
        } else {
            return true;
        }
    }
})


